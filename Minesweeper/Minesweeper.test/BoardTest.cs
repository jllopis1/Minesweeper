﻿using Minesweeper.lib;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Minesweeper.test
{
    public class BoardTest
    {
        
        [Fact]
        public void MinesSize()
        {
            //Arrange 
            Board board = new Board(4,4, FakeCreateMines());

            //Assert
            Assert.Equal(8,board.CountMines());
        }


        [Fact]
        public void EmptyBoard()
        {
            //Arrange
            Board board;

            //Assert
            Assert.Throws<ArgumentException>(() => board = new Board(0, 0, FakeCreateMines()));
        }


        private List<string> FakeCreateMines()
        {
            List<string> fakeMines = new List<string>
            {
                "00",
                "01",
                "02",
                "10",
                "11",
                "12",
                "20",
                "21",
                "22",
                "31"
            };

            return fakeMines;
        }
    }
}
