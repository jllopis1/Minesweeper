﻿using Minesweeper.lib;
using Minesweeper.lib.Game;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Minesweeper.test
{
    public class GameTest
    {

        [Fact]
        public void Win()
        {
            //Arrange
            Cell initialPosition = new Cell(3, 0, false);
            Game game = new Game(FakeCreateMines(), initialPosition);
           
            //Act
            game.Rigth();
            game.Rigth();
            game.Rigth();


            //Assert
            Assert.Equal(4, Game.Score);
            Assert.Equal(1, Game.LifeCount);
            Assert.Equal(game.Status, Game.EStatus.Survived.ToString());
        }


        [Fact]
        public void CheckFinalPositions()
        {
            //Arrange
            Game game = new Game();

            //Assert
            Assert.Equal(0, game.FinalPositions[0].X);
            Assert.Equal(3, game.FinalPositions[0].Y);
            Assert.Equal(1, game.FinalPositions[1].X);
            Assert.Equal(3, game.FinalPositions[1].Y);
            Assert.Equal(2, game.FinalPositions[2].X);
            Assert.Equal(3, game.FinalPositions[2].Y);
            Assert.Equal(3, game.FinalPositions[3].X);
            Assert.Equal(3, game.FinalPositions[3].Y);

        }


        [Fact]
        public void CheckExistingMine()
        {
            //Arrange 
            Cell initialPosition = new Cell(1, 3, true);

            //Assert
            Assert.True(initialPosition.IsMine);
        }


        [Fact]
        public void Loose()
        {
            //Arrange
            Cell initialPosition = new Cell(1, 0, false);
            Game game = new Game(FakeCreateMines(), initialPosition);

            //Act
            game.Rigth();

            //Assert
            Assert.Equal(2, Game.Score);
            Assert.Equal(0,Game.LifeCount);
            Assert.Equal(game.Status,Game.EStatus.GameOver.ToString());
        }


        [Fact]
        public void Score()
        {
            //Arrange
            Game game = new Game();

            //Act
            game.Rigth();
            game.Rigth();
            game.Rigth();

            //Assert
            Assert.Equal(3, Game.Score);
        }


        [Fact]
        public void OutsideBoard()
        {
            //Arrange
            Cell initialPosition = new Cell(1, 0, false);
            Game game = new Game(FakeCreateMines(), initialPosition);

            //Act
            game.Left();

            //Assert
            Assert.Equal(1,initialPosition.X);
            Assert.Equal(0, initialPosition.Y);
        }


        private List<string> FakeCreateMines()
        {
            List<string> fakeMines = new List<string>
            {
                "00",
                "12",
                "11",
                "20",
                "22"
            };

            return fakeMines;
        }
    }
}
