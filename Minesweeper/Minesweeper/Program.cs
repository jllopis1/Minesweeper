﻿using Minesweeper.lib.Game;
using System;
using System.Linq;

namespace Minesweeper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome To Schneider's Survival Minesweeper!");

            char input = 'S';
            while (input != 'Q')
            {
                RunTypeCommands();

                input = Console.ReadLine().ToUpper().First();

                if (input == 'P')
                {
                    PlayGame();
                }
            }

        }

        private static void PlayCommands()
        {
            Console.WriteLine("Here are the commands you can enter:");
            Console.WriteLine("N - NEW GANE");
            Console.WriteLine("Q - QUIT GAME");

        }

        private static void MovementInstructions()
        {

            Console.WriteLine("INSTRUCTIONS:");
            Console.WriteLine("U - UP");
            Console.WriteLine("D - DOWN");
            Console.WriteLine("R - RIGHT");
            Console.WriteLine("L - LEFT");
        }

        private static void RunTypeCommands()
        {
            Console.WriteLine("How do you want to play?");
            Console.WriteLine("P - Play Game");
            Console.WriteLine("Q - Quit");
        }

        private static void ActualStats()
        {
            Console.WriteLine($"Score: {Game.Score}");
            Console.WriteLine($"LifeCount: {Game.LifeCount}");
        }

        private static void PlayGame()
        {
            char input = 'S';

            Game game = null;

            while (input != 'Q')
            {
                if (game == null)
                {
                    game = new Game();

                    MovementInstructions();

                }
                    Console.WriteLine($"Current Position: {game.InitialPosition.X}{game.InitialPosition.Y}");

                    if (input.ToString() == "U")
                    {
                        game.MovementRules(input.ToString());
                        ActualStats();

                    }

                    if (input.ToString() == "D")
                    {
                        game.MovementRules(input.ToString());
                        ActualStats();
                    }

                    if (input.ToString() == "R")
                    {
                        game.MovementRules(input.ToString());
                        ActualStats();

                    }

                    if (input.ToString() == "L")
                    {
                        game.MovementRules(input.ToString());
                        ActualStats();
                    }

                    input = Console.ReadLine().ToUpper().First();

                    if (game.Status == Game.EStatus.Survived.ToString())
                    {
                        Console.WriteLine("You Survived, You win!!!");
                        Console.WriteLine("");
                        Console.WriteLine($"Current Position: {game.InitialPosition.X}{game.InitialPosition.Y}");
                        Console.WriteLine("");
                        ActualStats();
                        Console.WriteLine("");
                        PlayCommands();
                        break;

                    }

                    if (game.Status == Game.EStatus.GameOver.ToString())
                    {

                        Console.WriteLine("( *   *')");
                        Console.WriteLine("");
                        Console.WriteLine("BOOOMM!!!,You Died, You Loose!!!");
                        Console.WriteLine("");
                        ActualStats();
                        Console.WriteLine("");
                        PlayCommands();
                        break;

                    }

                    if (input == 'N')
                    {
                        game = null;
                    }
            }
        }
    }
}
