﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Minesweeper.lib.Game
{
    public class Game
    {
        public enum EStatus
        {
            GameOver,
            Survived,
            StartingPoint
        }

        public  Board Board { get; set; }

        public  Cell InitialPosition { get; set; }

        public  List<Cell> FinalPositions { get; set; }

        public static int LifeCount;

        public string Status { get; set; }

        public static int Score;

        
        public Game()
        {
            Score = 0;
            Status = EStatus.StartingPoint.ToString();
            LifeCount = 1;

            Board = new Board(4, 4, CreateMines());
            InitialPosition = Board.GetInitialPosition();
            FinalPositions = Board.GetFinalPositions();
        }

        //Constructor for GameTest
        public Game(List<string> mines, Cell cell)
        {
            Score = 1;
            Status = EStatus.StartingPoint.ToString();
            LifeCount = 1;

            Board = new Board(4, 4, mines);
            InitialPosition = cell;
            FinalPositions = Board.GetFinalPositions();
        }

        public void MovementRules(string input)
        {
            switch (input.ToUpper())
            {
                case "U":
                    Up();
                    break;

                case "D":
                    Down();
                    break;

                case "R":
                    Rigth();
                    break;

                case "L":
                    Left();
                    break;
                default:
                    Console.WriteLine("This Movement doesnt exist");
                    break;

            }
        }

        public void MakeMovement(int x, int y)
        {
            //Check Limits
            if (!Board.CheckIsOutsideBoard(InitialPosition))
            {
                InitialPosition = Board.GetCell(x, y);
                UpdateLifeCounter();
                Score++;
                Status = LifeCount > 0 ? Status : EStatus.GameOver.ToString();
            }

        }

        public void Up()
        {
            MakeMovement(InitialPosition.X - 1, InitialPosition.Y);
        }


        public void Down()
        {
            MakeMovement(InitialPosition.X + 1, InitialPosition.Y);
        }

        public void Rigth()
        {
            MakeMovement(InitialPosition.X, InitialPosition.Y + 1);
            if (Status != EStatus.GameOver.ToString())
            {
                if (CheckFinalPosition(InitialPosition.X, InitialPosition.Y))
                {
                    Status = EStatus.Survived.ToString();
                }
            }
        }

        public void Left()
        {
            MakeMovement(InitialPosition.X, InitialPosition.Y - 1);
        }

        public void UpdateLifeCounter()
        {
            if (InitialPosition.IsMine)
            {
                LifeCount--;
            }
        }

        public static List<string> CreateMines()
        {
            List<string> mines = new List<string>
            {
                "00",
                "11",
                "13",
                "20",
                "22"
            };
            return mines;
        }

        public bool CheckFinalPosition(int i, int j)
        {
            var result = false;
            foreach (var cell in FinalPositions)
            {
                if (cell.X == i && cell.Y == j)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}
