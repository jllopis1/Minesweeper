﻿using System;

namespace Minesweeper.lib
{
    public class Cell
    {
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsMine { get; set; }
        
        public Cell(int x, int y, bool isMine)
        {
            X = x;
            Y = y;
            this.IsMine = isMine;
        }
    }
}
