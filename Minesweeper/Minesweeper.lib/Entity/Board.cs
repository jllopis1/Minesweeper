﻿using System;
using System.Collections.Generic;

namespace Minesweeper.lib
{
    public class Board
    {
        public static List<Cell> Cells;
       
        public static int Height;

        public static int Width;

        public Board(int width, int height, List<string> mines)
        {
            Width = width;
            Height = height;
            Cells = CreateBoard(mines);
        }

        public List<Cell> CreateBoard(List<string> mines)
        {
            List<Cell> cells = new List<Cell>();
            int minesCounter  = (Width*Height) / 2;

            if (Height <= 0 || Width <= 0)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if (minesCounter > 0 && mines.Contains(i + "" + j))
                    {
                        cells.Add(new Cell(i, j, true));
                        minesCounter--;
                    }
                    else
                    {
                        cells.Add(new Cell(i, j, false));
                    }
                }
            }

            return cells;
        }


        public  Cell GetInitialPosition()
        {
            int i;
            var arrayCells = Cells.ToArray();
            var random = new Random();
            var initialPositions = new List<Cell>();
            for (i = 0; i < arrayCells.Length; i++)
            {
                if (arrayCells[i].Y == 0 && !arrayCells[i].IsMine)
                {
                    initialPositions.Add(arrayCells[i]);
                }
            }

            var start = random.Next(initialPositions.ToArray().Length);
            return initialPositions[start];
        }


        public List<Cell> GetFinalPositions()
        {
            int i;
            var finalPositions = new List<Cell>();
            foreach (var cell in Cells)
            {
                if (cell.Y == Width-1)
                {
                    finalPositions.Add(cell);
                }
            }

            return finalPositions;
        }


        public Cell GetCell(int i, int j)
        {
            Cell result = new Cell(0, 0, false);

            foreach (var cell in Cells)
            {
                if (cell.X == i && cell.Y == j)
                {
                    result = cell;
                    break;
                }
            }

            return result;
        }


        public bool CheckIsOutsideBoard(Cell currentCell)
        {
            return currentCell.X >= Height || currentCell.X < 0 || currentCell.Y < 0;
        }

        public int CountMines()
        {
            int i = 0;

            foreach (var cell in Cells)
            {
                if (cell.IsMine)
                {
                    i++;
                }
            }

            return i;
        }
    }
}
